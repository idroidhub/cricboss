import React from 'react';
import Constants from './Constants';
import {Platform, View} from 'react-native';
import {BannerAd, BannerAdSize} from '@react-native-firebase/admob';

export default class MAdLargeBanner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            banner_id: Platform.OS === 'ios' ? Constants.IOS_AD_BANNER : Constants.ANDROID_AD_BANNER,
        };
    }

    render() {
        // size={BannerAdSize.LARGE_BANNER}
        if (!Constants.SHOW_ADS) return (<View></View>);
        return (
            <BannerAd
                unitId={this.state.banner_id}
                style={{alignSelf : 'center'}}
                size={BannerAdSize.LARGE_BANNER}
                onAdLoaded={() => {
                    console.log('Large Banner loaded');
                }}
            />
        );
    }
}

