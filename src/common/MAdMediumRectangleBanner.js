import React from 'react';
import Constants from './Constants';
import {Platform, View} from 'react-native';

import {BannerAd, BannerAdSize} from '@react-native-firebase/admob';

export default class MAdMediumRectangleBanner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            banner_id: Platform.OS === 'ios' ? Constants.IOS_AD_BANNER_LARGE : Constants.ANDROID_AD_BANNER_LARGE,
        };

    }

    render() {
        // size={BannerAdSize.MEDIUM_RECTANGLE}
        if (!Constants.SHOW_ADS) return (<View></View>);
        return (
            <BannerAd
                unitId={this.state.banner_id}
                style={{alignSelf : 'center'}}
                size={BannerAdSize.MEDIUM_RECTANGLE}
                onAdLoaded={() => {
                    console.log('Medium Rectangle loaded');
                }}
            />
        );
    }
}

