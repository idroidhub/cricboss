import Constants from "./Constants";





const ACCESS_TOKEN = 'ACCESSTOKEN'+ Constants.RN_CODEBASE_VERSION;;
const NAME = 'NAME'+ Constants.RN_CODEBASE_VERSION;;
const PROFILE_PIC = 'PROFILE_PIC'+ Constants.RN_CODEBASE_VERSION;;


export default {
    ACCESS_TOKEN,
    NAME,
    PROFILE_PIC
};