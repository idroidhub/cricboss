import AsyncStorage from '@react-native-community/async-storage';


export async function setPrefs(key, value) {
    return await AsyncStorage.setItem(key, value);
}


export async function getPrefs(key) {
    return await AsyncStorage.getItem(key);
}
