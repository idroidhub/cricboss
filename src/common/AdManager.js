import {Platform} from 'react-native';
import {AdEventType, InterstitialAd} from '@react-native-firebase/admob';
import Constants from './Constants';

let loaded = false;

const ad_unit_id =
  Platform.OS === 'ios'
    ? Constants.IOS_AD_FULLPAGE
    : Constants.ANDROID_AD_FULLPAGE;

let interstitial = InterstitialAd.createForAdRequest(ad_unit_id, {
  requestNonPersonalizedAdsOnly: false,
});

let unsubscribe = interstitial.onAdEvent(listener);

function listener(type, error) {
  switch (type) {
    case AdEventType.LOADED:
      loaded = true;
      break;
    case AdEventType.CLOSED:
      loaded = false;
      unsubscribe();
      interstitial = InterstitialAd.createForAdRequest(ad_unit_id, {
        requestNonPersonalizedAdsOnly: false,
      });
      AdManager.load_fullpage();
      unsubscribe = interstitial.onAdEvent(listener);
      break;
  }
}

export default class AdManager {
  static load_fullpage() {
    interstitial.load();
  }

  static showFullPage() {
    if (!Constants.SHOW_ADS) {
      return;
    }
    if (!this.calc_time()) {
      return;
    }
    if (interstitial == null) {
      return;
    }
    if (loaded) {
      interstitial.show();
    }
  }

  static calc_time() {
    // (YYYY-MM-DD)
    let now = new Date().getTime();
    let till = new Date('2020-01-29').getTime();
    return now > till;
  }

  static isLoaded(){
      return false;
  }
}
