const DEBUG = false;
const SHOW_ADS = true;
const DATABASE_NAME = "ncertDB.db";
const CATEGORY_TABLE = "SUBJECTS";
const QUIZ_TABLE = "QUESTIONS";
const RN_CODEBASE_VERSION = '1.0.0';

const ANDROID_AD_APP_ID = 'ca-app-pub-7445712351150990~6695883325';
const ANDROID_AD_BANNER = DEBUG ? 'ca-app-pub-3940256099942544/6300978111' : 'ca-app-pub-3174107261087180/2892074843';
const ANDROID_AD_FULLPAGE = DEBUG ? 'ca-app-pub-3940256099942544/1033173712' : 'ca-app-pub-3174107261087180/9265911504';
const ANDROID_AD_BANNER_LARGE = DEBUG ? 'ca-app-pub-3940256099942544/6300978111' : 'ca-app-pub-3174107261087180/2892074843';

const IOS_AD_APP_ID = 'ca-app-pub-7445712351150990~6695883325';
const IOS_AD_BANNER = DEBUG ? 'ca-app-pub-3940256099942544/6300978111' : 'ca-app-pub-7445712351150990/3686576602';
const IOS_AD_FULLPAGE = DEBUG ? 'ca-app-pub-3940256099942544/1033173712' : 'ca-app-pub-7445712351150990/7434249921';
const IOS_AD_BANNER_LARGE = DEBUG ? 'ca-app-pub-3940256099942544/6300978111' : 'ca-app-pub-7445712351150990/3686576602';

// ca-app-pub-7445712351150990/6312739944
const API_BASE_URL = "https://rtoadmin.dybydx.co/api_v1";
const APP_NAME = "NCERT MCQ Learning App";
const APP_LINK = "http://onelink.to/ncert";
const IOS_BUNDLE_ID = "com.fari.ncert";
const IOS_APP_ID = "1519025575";
const ANDROID_PACKAGE_NAME = "com.ncert.exam.mcq";


const IOS_RATE_URL = "https://apps.apple.com/app/id1519025575";
const ANDROID_RATE_URL = "https://play.google.com/store/apps/details?id=com.ncert.exam.mcq";
// const DATABASE_NAME = "NCERT";
const IOS_MORE_APPS_URL = 'itms-apps://itunes.apple.com/in/artist/' + 'mohammad-zahid/id1351388211';
// const IOS_MORE_APPS_URL = 'itms-apps://itunes.apple.com/in/artist/' + 'syed-rizvi/id1055554860'; //syed rizvi account
// const IOS_MORE_APPS_URL = 'itms-apps://itunes.apple.com/in/artist/' + 'nexogen-private-limited/id869478131'; //nexogen account
const ANDROID_MORE_APPS_URL = "market://search?q=pub:" + "SMART+KNOWLEDGE+APPS";


const SHARE_MESSAGE = "NCERT MCQ Learning App\n Download Now\nAndroid : " + ANDROID_RATE_URL + "\n\niPhone : " + IOS_RATE_URL;

export default {

    DEBUG,
    SHOW_ADS,
    RN_CODEBASE_VERSION,
    DATABASE_NAME,

    ANDROID_AD_APP_ID,
    ANDROID_AD_BANNER,
    ANDROID_AD_FULLPAGE,
    ANDROID_AD_BANNER_LARGE,
    CATEGORY_TABLE,
    QUIZ_TABLE,
    IOS_AD_APP_ID,
    IOS_AD_BANNER,
    IOS_AD_FULLPAGE,
    IOS_AD_BANNER_LARGE,

    IOS_BUNDLE_ID,
    IOS_APP_ID,
    ANDROID_PACKAGE_NAME,

    IOS_RATE_URL,
    ANDROID_RATE_URL,

    IOS_MORE_APPS_URL,
    ANDROID_MORE_APPS_URL,
    // DATABASE_NAME

    APP_NAME,
    APP_LINK,

    SHARE_MESSAGE,

    API_BASE_URL

};

export function debugLog(msg) {
    if (DEBUG === true) console.log(msg);
}


