const tintColor = '#66ffff';

const GREEN = '#14969a';

const BLUE = '#646ee3';
// const MY_BLACK = '#13212A';
// const CARD_COLOR = '#182A38';
const MY_BLACK = '#1d1d1d';
const CARD_COLOR = '#121212';
const TEXT_COLOR = '#FFFFFF';
const WHITE = '#FFFFFF';
const TEAM_IMG_BORDER_CLR = '#666';

export default {
  tintColor,
  tabIconDefault: '#384659',
  tabIconSelected: tintColor,
  tabBar: '#66ffff',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  selected_text_color: '#66ffff',
  disabled_text_color: '#384659',
  background: '#000827',
  secondaryBackground: '#1a1f3c',
  colorRateModal: '#ff005a',
  GREEN,
MY_BLACK,
TEXT_COLOR,
  BLUE,
  WHITE,
  TEAM_IMG_BORDER_CLR,
  CARD_COLOR
};
