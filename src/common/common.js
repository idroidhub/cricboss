import {Alert, BackHandler, Linking, Platform, Share} from 'react-native';
import Constants from "./Constants";


export function rateApp() {
    const rate_url = Platform.OS === 'ios' ? Constants.IOS_RATE_URL : Constants.ANDROID_RATE_URL;
    Linking.canOpenURL(rate_url).then(supported => {
        supported && Linking.openURL(rate_url);
    }, (err) => console.log(err));
}

export function openUrl(url) {
    Linking.canOpenURL(url).then(supported => {
        supported && Linking.openURL(url);
    }, (err) => console.log(err));
}

export function moreApps() {
    const moreAppsUrl = Platform.OS === 'ios' ? Constants.IOS_MORE_APPS_URL : Constants.ANDROID_MORE_APPS_URL;
    Linking.canOpenURL(moreAppsUrl).then(supported => {
        supported && Linking.openURL(moreAppsUrl);
    }, (err) => console.log(err));
}

export function share() {
    Share.share({title: Constants.APP_NAME, message: Constants.SHARE_MESSAGE})
        .then(({action, activityType}) => {
            console.log(action == Share.sharedAction, activityType);
        });
}

export function shareText(msg) {
    Share.share({title: Constants.APP_NAME, message: msg+Constants.SHARE_MESSAGE})
        .then(({action, activityType}) => {
            console.log(action == Share.sharedAction, activityType);
        });
}

export function shareViaWhatsapp(text) {
    const url = `whatsapp://send?text=${text}`
    Linking.canOpenURL(url).then(supported => {
        supported && Linking.openURL(url);
    }, (err) => console.log(err));
}

export const exitAlert = () => {
    Alert.alert(
        'Confirm exit',
        'Do you want to quit the app?',
        [
            {text: 'Rate App', onPress: () => rateApp()},
            {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
            {text: 'Yes', onPress: () => BackHandler.exitApp()},
        ],
        {cancelable: false},
    );

};


export const shuffleArray=(array) =>{
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}