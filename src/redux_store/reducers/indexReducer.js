import {
  SET_LOADING,
  ON_ACCESS_TOKEN,
  ON_PROFILE_PIC,
  ON_NAME,
  SET_MCQ,
  ON_CURRENT_INDEX,
  SET_WRONG_COUNT,
  SET_RIGHT_COUNT,
  SET_PLAY_TIME,
  ON_COLOR_CHANGE,
   SET_LOWER_IMAGE, SET_DIALOG, ON_LOADING_TEXT
} from '../actions/types';

const initialState = {
  is_loading: true,

};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        is_loading: action.payload,
      };

 
    default:
      return state;
  }
}
