import React from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  TextInput,
  View,
  Dimensions,
  FlatList,
  ActivityIndicator,
  Image,
  ImageBackground,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';



import { getWidth, width, height } from '../common/CommonFunction';
import Colors from '../common/Colors';
import MyFont from '../common/MyFont';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
  }



  render() {
    return (

      <View style={{ flex: 1, backgroundColor: Colors.MY_BLACK, justifyContent: "center", alignItems: "center" }}>


        <View style={[styles.shadow, {
          width: "85%", padding: 20, backgroundColor: Colors.CARD_COLOR, borderRadius: 10,

        }]}>
          <Text style={[styles.upcomingTeamText, { marginBottom: 10 }]}>
            {"Tomorrow, 8PM, T20"}
          </Text>

          <View style={{ alignItems: "center" }}>


            <View style={{ width: '100%', flexDirection: "row", alignItems: "center" }}>

              <Image

                source={{ uri: 'https://thumbs.dreamstime.com/b/flag-india-15425515.jpg' }}
                style={styles.upcomingTeamImg}

              />

              <Text style={styles.upcomingTeamText}>
                {"INDIA"}
              </Text>
            </View>

            <View style={{ width: '90%', height: 1, backgroundColor: Colors.WHITE }} />
            <View style={{ width: '100%', flexDirection: "row", alignItems: "center", }}>
              <Image

                source={{ uri: 'https://cdn4.iconfinder.com/data/icons/square-world-flags/180/afghanistan_flag_square-512.png' }}
                style={styles.upcomingTeamImg}

              />
              <Text style={styles.upcomingTeamText}>
                {"AFGHANISTAN"}
              </Text>
            </View>
          </View>

        </View>



        <View style={[styles.shadow, {
          marginTop: 20,
          width: "85%", padding: 20, height: 100, backgroundColor: Colors.CARD_COLOR, borderRadius: 10,
          justifyContent: "center",
        }]}>
          <Text style={[styles.upcomingTeamText, { marginBottom: 10 }]}>
            {"Tomorrow, 8PM, T20"}
          </Text>
          <View style={{ justifyContent: "space-between", alignItems: "center", flexDirection: "row" }}>


            <View style={{ width: '45%', flexDirection: "row", alignItems: "center" }}>

              <Image

                source={{ uri: 'https://thumbs.dreamstime.com/b/flag-india-15425515.jpg' }}
                style={styles.upcomingTeamImg}

              />

              <Text style={styles.upcomingTeamText}>
                {"INDIA"}
              </Text>
            </View>

            <View style={{ width: 1, height: 30, backgroundColor: Colors.WHITE }} />
            <View style={{ width: '45%', flexDirection: "row", alignItems: "center", }}>
              <Image

                source={{ uri: 'https://cdn4.iconfinder.com/data/icons/square-world-flags/180/afghanistan_flag_square-512.png' }}
                style={styles.upcomingTeamImg}

              />
              <Text style={styles.upcomingTeamText}>
                {"AFGHANISTAN"}
              </Text>
            </View>
          </View>

        </View>


      </View>

    );
  }
}


const styles = StyleSheet.create({

  upcomingTeamText: { fontFamily: MyFont.FontFamily, color: Colors.TEXT_COLOR, marginLeft: 5 },

  upcomingTeamImg: { width: 30, height: 30, borderRadius: 15, borderColor: Colors.TEAM_IMG_BORDER_CLR, borderWidth: 1 },







  text: {
    color: 'red',
    fontSize: 20,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width,
    height,
  },
  row_style: {
    flex: 1,
    backgroundColor: 'gray',
    height: 100,
    margin: 10,
  },
  search_style: {
    width: width - 10,
    margin: 5,
    paddingHorizontal: 10,

    borderWidth: 2,
    borderRadius: 2,
    borderColor: 'gray',
    justifyContent: 'center',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.4,

    elevation: 5,
  },
  upper: {
    color: 'gray',

    alignSelf: 'center',
    textAlign: 'center',
  },
});

const mapStateToProps = (state) => {
  const s = state.indexReducer;
  return {
    is_loading: s.is_loading,
    name: s.name,
    profile_pic: s.profile_pic,
    access_token: s.access_token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {

    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
