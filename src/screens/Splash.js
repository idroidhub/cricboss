import React from 'react';
import {
  StyleSheet,
  Text,
  SafeAreaView,
  TextInput,
  View,
  Dimensions,
  FlatList,
  ActivityIndicator,
  Image,
  ImageBackground,
} from 'react-native';

const timer = require('react-native-timer');

import {run_database_migrations} from '../db/AppDatabase';
import {StackActions} from '@react-navigation/native';
import Colors from '../common/Colors';

class Splash extends React.Component {
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    // run_database_migrations().then(() => {
    //   debugLog('Execured all');
    // });
    // AdManager.load_fullpage();

    timer.setTimeout(
      this,
      'splash',
      () => {
        this.props.navigation.dispatch(StackActions.replace('HomeScreen', {}));
      },
      1500,
    );
  }

  render() {
    return (

      <View style={{flex:1,backgroundColor:Colors.BLUE}}>
<Text>
  {"Hello"}
</Text>

      </View>
  
     
    );
  }
}

export const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  text: {
    color: 'red',
    fontSize: 20,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width,
    height,
  },
  row_style: {
    flex: 1,
    backgroundColor: 'gray',
    height: 100,
    margin: 10,
  },
  search_style: {
    width: width - 10,
    margin: 5,
    paddingHorizontal: 10,

    borderWidth: 2,
    borderRadius: 2,
    borderColor: 'gray',
    justifyContent: 'center',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.4,

    elevation: 5,
  },
  upper: {
    color: 'gray',

    alignSelf: 'center',
    textAlign: 'center',
  },
});

export default Splash;
