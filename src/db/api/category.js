import Constants, {debugLog} from '../../common/Constants';
import {executeSql} from '../DatabaseExecutor';

//language_ids should be in format [1,2];
export const fetch_category_n_insert = async (data,base_url) => {
    // debugLog('klklk');

  if (data.length === 0) {
    debugLog('sections ids not passed');
    return;
  }
  await insert_category(data,base_url);
};

const insert_category = async (categories = [],base_url) => {
  const table_name = Constants.CATEGORY_TABLE;

  debugLog('insertCategory()s');
  let query = `insert into ${table_name} `;
  query += `(id, name,  lang_id, image_path,is_active,updated_at,total_question,attempted_question) `;
  query += `VALUES (?,  ?, ?, ?, ?, ?, ?, ?)`;

  if (categories.length === 0) {
    debugLog('insertCategory() - items array is empty');
  }

  try {
    const asyncRes = await Promise.all(
      categories.map(async item => {
        let result = await executeSql(
          `select * from ${table_name} where id = ${item.id}`,
          [],
        );
        let array = result.rows;

        if (array.length === 0) {
          await executeSql(query, [
            item.id,
            item.name,
            item.lang_id,
            base_url+item.image,
            item.is_active,
            item.updated_at,
            50,
            0, //for new posts
          ]);
        }
      }),
    );
    debugLog(`successfully inserts in ${table_name}`);
  } catch (error) {
    debugLog(`insert error in ${table_name} :` + JSON.stringify(error));
  }
};

export const get_category = async () => {
  const table_name =Constants.CATEGORY_TABLE;
  let query = `select * from ${table_name}`;
  debugLog(query);
  try {
    let result = await executeSql(query, []);
    let rows = result.rows;
    let array = [];
    for (let i = 0; i < rows.length; i++) {
      array.push(rows.item(i));
    }
    // debugLog(array);
    return array;
  } catch (error) {
    debugLog(`get_category() error in ${table_name} :` + JSON.stringify(error));
  }
};

export async function update_count(id,count) {
  const table_name = Constants.CATEGORY_TABLE;
  let query = `update ${table_name} set total_question = ${count} where id = ${id}`;
  try {
    await executeSql(query, []);
    debugLog(`updated selected value`);
  } catch (error) {
    debugLog(
      `toggle_selected_category error in ${table_name} :` +
        JSON.stringify(error),
    );
  }
}


