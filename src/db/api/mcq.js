import Constants, {debugLog} from '../../common/Constants';
import {executeSql} from '../DatabaseExecutor';

//language_ids should be in format [1,2];
export const fetch_mcq_n_insert = async (data,cat_id) => {
    

  if (data.length === 0) {
    debugLog('sections ids not passed');
    return;
  }
  await insert_mcq(data,cat_id);
};

const insert_mcq = async (mcq = [],cat_id) => {
  const table_name = Constants.QUIZ_TABLE;
  let query = `insert into ${table_name} `;
  query += `(id,  lang_id, question,que_img,op_a,op_b,op_c,op_d,img_a,img_b,img_c,img_d,answer,explanation,category_id,level_id,added_by,created_at,is_active,is_attempted,updated_at) `;
  query += `VALUES (?,?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?, ?, ?, ?,?,?,?,?,?)`;

  if (mcq.length === 0) {
    debugLog('insertCategory() - items array is empty');
  }
  try {
    const asyncRes = await Promise.all(
      mcq.map(async item => {
        // let result = await executeSql(
        //   `select * from ${table_name} where id = ${item.id}`,
        //   [],
        // );
        // let array = result.rows;
        // if (array.length === 0)
        debugLog(item);
         {
          await executeSql(query, [
            item.id,
            item.lang_id,
            item.question,
           item.que_img,
            item.op_a,
            item.op_b,
            item.op_c,
            item.op_d,
            item.img_a,
            item.img_b,
            item.img_c,
            item.img_d,
            item.answer,
            item.explanation,
            cat_id,
            item.level_id,
            item.added_by,
            item.created_at,
            item.is_active,
           0,
            item.updated_at
        
          ]);
        }
      }),
    );
    debugLog(`successfully inserts in ${mcq.length}`);
  } catch (error) {
    debugLog(`insert error in ${table_name} :` + JSON.stringify(error));
  }
};

export const get_mcq = async (cat_id) => {
  const table_name =Constants.QUIZ_TABLE;
  let query = `select * from ${table_name} where is_attempted= 0 AND category_id=${cat_id} LIMIT 10`;

  try {
    let result = await executeSql(query, []);
    let rows = result.rows;
    let array = [];
    for (let i = 0; i < rows.length; i++) {
      array.push(rows.item(i)); 
    }
    // debugLog(array.length);
    return array;
  } catch (error) {
    debugLog(`get_mcq() error in ${table_name} :` + JSON.stringify(error));
  }
};

export const get_last_mcq = async (cat_id) => {
  const table_name =Constants.QUIZ_TABLE;
  let query = `select * from ${table_name}  where category_id=${cat_id} ORDER BY id DESC`;

  try {
    let result = await executeSql(query, []);
    let rows = result.rows;
    let array = [];
    for (let i = 0; i < rows.length; i++) {
      array.push(rows.item(i));
    }
    // debugLog(array);
   

    return array;
  } catch (error) {
    debugLog(`get_mcq() error in ${table_name} :` + JSON.stringify(error));
  }
};

export async function update_attempted(id,cat_id) {

  
  const quiz_table = Constants.QUIZ_TABLE;
  let query_quiz = `update ${quiz_table} set is_attempted = 1 where id = ${id}`;
  
  const table_name = Constants.CATEGORY_TABLE;
  let query = `update ${table_name} set attempted_question = attempted_question+1 where id = ${cat_id}`;

  debugLog(query_quiz+" "+query);
  try {
    await executeSql(query, []);
    await executeSql(query_quiz, []);
    debugLog(`updated selected value`);
  } catch (error) {
    debugLog(
      `toggle_selected_category error in ${table_name} :` +
        JSON.stringify(error),
    );
  }
}


