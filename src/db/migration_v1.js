import {debugLog} from '../common/Constants';
import {is_migration_registered, register_migration} from './migrations';
import {executeSql} from './DatabaseExecutor';

import Constants from '../common/Constants';
const migration_name = `create_base_tables`;

export const run_migration_v1 = async () =>{
    let is_registered = await is_migration_registered(migration_name);
    if (is_registered) {
        debugLog('already migrated : ' + migration_name);
        return;
    }

    await create_table_mcq();
    await create_table_counts();
    await create_table_subject_category();
    await create_table_subject_mcq();
    await create_table_set_ids();
    await register_migration(migration_name);
};


const create_table_mcq = async (db) =>{
    let table_name = 'mcq';
    let query = `create table if not exists ${table_name}(`;
    query += `id integer primary key not null, `;
    query += `lang_id integer, `;
    query += `question text, `;
    query += `op_a text, `;
    query += `op_b text, `;
    query += `op_c text, `;
    query += `op_d text, `;
    query += `answer integer, `;
    query += `level_id integer, `;
    query += `is_attempt integer default 0)`;

    try{
        let results = await executeSql(query,[]);
        debugLog(`created ${table_name} success`);
    }catch (error) {
        debugLog(`creation error on ${table_name}` + JSON.stringify(error));
        return false;
    }
};

const create_table_counts = async (db) =>{
    let table_name = 'counts';
    let query = `create table if not exists ${table_name}(`;
    query += `id integer primary key not null, `;
    query += `key text unique, `;
    query += `value integer default 0)`;


    try{
        let results = await executeSql(query,[]);
        debugLog(`created ${table_name} success`);
    }catch (error) {
        debugLog(`creation error on ${table_name}` + JSON.stringify(error));
        return false;
    }
};

const create_table_set_ids = async (db) =>{
    let table_name = 'set_ids';
    let query = `create table if not exists ${table_name}(`;
    query += `id integer primary key not null, `;
    query += `is_attempt integer,`;
    query += `star integer)`;
    
    try{
        let results = await executeSql(query,[]);
        console.log(`created  set)ids success ` + JSON.stringify(results));
    }catch (error) {
      console.log(`creation set error on ` + JSON.stringify(error));
    }
    
};
const create_table_subject_category = async (db) =>{
    let query = `create table if not exists ${Constants.CATEGORY_TABLE}(`;
    query += `id integer primary key not null, `;
    query += `name text,`;
    query += `lang_id integer,`;
    query += `image_path text,`;
    query += `is_active integer,`;
    query += `total_question integer,`;
    query += `attempted_question integer,`;
    query += `updated_at text)`;
    
    try{
        let results = await executeSql(query,[]);
        console.log(`created  success ` + JSON.stringify(results));
    }catch (error) {
      console.log(`creation error on ` + JSON.stringify(error));
    }
    
};

const create_table_subject_mcq = async (db) =>{
let query2 = `create table if not exists ${Constants.QUIZ_TABLE}(`;
query2 += `id integer primary key not null, `;

query2 += `lang_id integer,`;
query2 += `question text,`;
query2 += `que_img text,`;
query2 += `op_a text,`;
query2 += `op_b text,`;
query2 += `op_c text,`;
query2 += `op_d text,`;
query2 += `img_a text,`;
query2 += `img_b text,`;
query2 += `img_c text,`;
query2 += `img_d text,`;
query2 += `answer integer,`;
query2 += `explanation text,`;
query2 += `category_id integer,`;
query2 += `level_id integer,`;
query2 += `added_by integer,`;
query2 += `created_at text,`;
query2 += `is_active integer,`;
query2 += `is_attempted integer,`;
query2 += `updated_at text)`;



try{
    let results = await executeSql(query2,[]);
    console.log(`created  success ` + JSON.stringify(results));
}catch (error) {
  console.log(`creation error on ` + JSON.stringify(error));
}
};

