
import * as React from 'react';
import {Platform, StatusBar, StyleSheet, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/screens/HomeScreen';

import {Provider} from 'react-redux';
import store from './src/redux_store/store';
import Splash from './src/screens/Splash';



const Stack = createStackNavigator();

function App() {
  return (
      <Provider store = { store }>
          <View style={styles.container}>

              {/* <StatusBar backgroundColor={"#ffa"}/> */}

              {Platform.OS === 'ios' && <StatusBar
              translucent
                  backgroundColor="#5E8D48"
                  barStyle="dark-content"
              />}

              <NavigationContainer>

                  <Stack.Navigator screenOptions={{headerShown: false}}>
                  <Stack.Screen name="Splash" component={Splash}/>
                
         
                   <Stack.Screen name="HomeScreen" component={HomeScreen}/>
                 
                  </Stack.Navigator>
              </NavigationContainer>

          </View>
      </Provider>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

export default App;
